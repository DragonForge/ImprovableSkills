package org.zeith.improvableskills.init;

import com.zeitheron.hammercore.client.gui.IGuiCallback;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.zeith.improvableskills.client.gui.abil.anvil.ContainerSkilledAnvil;
import org.zeith.improvableskills.client.gui.abil.anvil.GuiSkilledAnvil;
import org.zeith.improvableskills.client.gui.abil.crafter.ContainerCrafter;
import org.zeith.improvableskills.client.gui.abil.crafter.GuiCrafter;
import org.zeith.improvableskills.client.gui.abil.ench.ContainerEnchPowBook;
import org.zeith.improvableskills.client.gui.abil.ench.ContainerPortableEnchantment;
import org.zeith.improvableskills.client.gui.abil.ench.GuiEnchPowBook;
import org.zeith.improvableskills.client.gui.abil.ench.GuiPortableEnchantment;

public class GuiHooksIS
{
	public static final IGuiCallback ENCHANTMENT = new IGuiCallback()
	{
		@Override
		public Object getServerGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new ContainerPortableEnchantment(player.inventory, world);
		}
		
		@Override
		public Object getClientGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new GuiPortableEnchantment(player.inventory, world);
		}
	};
	public static final IGuiCallback CRAFTING = new IGuiCallback()
	{
		@Override
		public Object getServerGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new ContainerCrafter(player.inventory);
		}
		
		@Override
		public Object getClientGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new GuiCrafter(player.inventory);
		}
	};
	public static final IGuiCallback ANVIL = new IGuiCallback()
	{
		@Override
		public Object getServerGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new ContainerSkilledAnvil(player.inventory, world, pos, player);
		}
		
		@Override
		public Object getClientGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new GuiSkilledAnvil(player.inventory, world);
		}
	};
	public static final IGuiCallback ENCH_POWER_BOOK_IO = new IGuiCallback()
	{
		@Override
		public Object getServerGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new ContainerEnchPowBook(player, world);
		}
		
		@Override
		public Object getClientGuiElement(EntityPlayer player, World world, BlockPos pos)
		{
			return new GuiEnchPowBook(player, world);
		}
	};
}