package org.zeith.improvableskills.items;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.utils.SoundUtil;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;
import org.zeith.improvableskills.data.PlayerDataManager;
import org.zeith.improvableskills.net.PacketScrollUnlockedSkill;

import java.util.ArrayList;
import java.util.List;

public class ItemSkillScrollCreative extends Item
{
	public ItemSkillScrollCreative()
	{
		setTranslationKey("scroll_creative");
		setMaxStackSize(1);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		if(!worldIn.isRemote)
			return PlayerDataManager.handleDataSafely(playerIn, data ->
			{
				List<ResourceLocation> loc = new ArrayList<>();
				for(PlayerSkillBase base : GameRegistry.findRegistry(PlayerSkillBase.class).getValues())
					if(base.getScrollState().hasScroll())
					{
						if(!data.stat_scrolls.contains(base.getRegistryName().toString()))
						{
							data.stat_scrolls.add(base.getRegistryName().toString());
							loc.add(base.getRegistryName());
						}
					}
				
				if(loc.isEmpty())
					return new ActionResult<ItemStack>(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
				
				ItemStack used = playerIn.getHeldItem(handIn).copy();
				playerIn.getHeldItem(handIn).shrink(1);
				
				int slot = handIn == EnumHand.OFF_HAND ? -2 : playerIn.inventory.currentItem;
				data.sync();
				HCNet.swingArm(playerIn, handIn);
				SoundUtil.playSoundEffect(worldIn, "block.enchantment_table.use", playerIn.getPosition(), .5F, 1F, SoundCategory.PLAYERS);
				if(playerIn instanceof EntityPlayerMP)
					HCNet.INSTANCE.sendTo(new PacketScrollUnlockedSkill(slot, used, loc.toArray(new ResourceLocation[0])), (EntityPlayerMP) playerIn);
				
				return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
			}, new ActionResult<ItemStack>(EnumActionResult.PASS, playerIn.getHeldItem(handIn)));
		return new ActionResult<ItemStack>(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
	}
}