package org.zeith.improvableskills.net;

import com.zeitheron.hammercore.internal.GuiManager;
import com.zeitheron.hammercore.net.IPacket;
import com.zeitheron.hammercore.net.PacketContext;
import net.minecraft.entity.player.EntityPlayerMP;
import org.zeith.improvableskills.data.PlayerDataManager;
import org.zeith.improvableskills.init.AbilitiesIS;
import org.zeith.improvableskills.init.GuiHooksIS;

public class PacketOpenPortableAnvil implements IPacket
{
	static
	{
		IPacket.handle(PacketOpenPortableAnvil.class, PacketOpenPortableAnvil::new);
	}
	
	@Override
	public void executeOnServer2(PacketContext net)
	{
		EntityPlayerMP mp = net.getSender();
		PlayerDataManager.handleDataSafely(mp, dat ->
		{
			if(dat.abilities.contains(AbilitiesIS.ANVIL.getRegistryName().toString()))
				GuiManager.openGuiCallback(GuiHooksIS.ANVIL, mp, mp.world, mp.getPosition());
		});
	}
}