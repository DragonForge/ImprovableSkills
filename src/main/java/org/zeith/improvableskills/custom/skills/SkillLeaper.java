package org.zeith.improvableskills.custom.skills;

import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillLeaper extends PlayerSkillBase
{
	public SkillLeaper()
	{
		super(15);
		setRegistryName(InfoIS.MOD_ID, "leaper");
		
		xpCalculator.xpValue = 2;
	}
}