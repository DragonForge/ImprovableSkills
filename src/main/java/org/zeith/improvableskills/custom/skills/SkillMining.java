package org.zeith.improvableskills.custom.skills;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.IDigSpeedAffectorSkill;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillMining extends PlayerSkillBase implements IDigSpeedAffectorSkill
{
	public SkillMining()
	{
		super(25);
		setRegistryName(InfoIS.MOD_ID, "mining");
		
		xpCalculator.baseFormula = "sqrt(%lvl%^3)*3";
	}
	
	@Override
	public float getDigMultiplier(ItemStack stack, BlockPos pos, PlayerSkillData data)
	{
		if(pos == null)
			return 0F;
		Block b = data.player.world.getBlockState(pos).getBlock();
		String obj = b.getHarvestTool(data.player.world.getBlockState(pos));
		if(stack.isEmpty() || obj == null)
			return 0.0F;
		boolean isQ = false;
		String[] strs = stack.getItem().getToolClasses(stack).toArray(new String[0]);
		for(String s : strs)
			if(s.equalsIgnoreCase("pickaxe"))
			{
				isQ = true;
				break;
			}
		return obj.equalsIgnoreCase("pickaxe") && isQ ? data.getSkillLevel(this) / 8F : 0F;
	}
}