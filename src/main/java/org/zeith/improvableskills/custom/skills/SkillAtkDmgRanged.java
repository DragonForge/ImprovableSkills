package org.zeith.improvableskills.custom.skills;

import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillAtkDmgRanged extends PlayerSkillBase
{
	public SkillAtkDmgRanged()
	{
		super(15);
		setRegistryName(InfoIS.MOD_ID, "atkdmg_ranged");
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 40;
		getLoot().setLootTable(LootTableList.ENTITIES_SKELETON);
		
		xpCalculator.xpValue = 3;
	}
}