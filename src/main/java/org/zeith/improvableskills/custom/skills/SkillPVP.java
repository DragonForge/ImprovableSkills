package org.zeith.improvableskills.custom.skills;

import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillPVP extends PlayerSkillBase
{
	public SkillPVP()
	{
		super(20);
		setRegistryName(InfoIS.MOD_ID, "pvp");
		xpCalculator.xpValue = 2;
	}
}