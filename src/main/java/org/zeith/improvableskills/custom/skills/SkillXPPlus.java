package org.zeith.improvableskills.custom.skills;

import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillXPPlus extends PlayerSkillBase
{
	public SkillXPPlus()
	{
		super(10);
		setRegistryName(InfoIS.MOD_ID, "xp+");
		
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 3;
		getLoot().setLootTable(LootTableList.ENTITIES_ELDER_GUARDIAN);
		
		xpCalculator.baseFormula = "%lvl%^3+(%lvl%+1)*100";
	}
}