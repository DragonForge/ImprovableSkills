package org.zeith.improvableskills.custom.skills;

import net.minecraft.world.storage.loot.LootTableList;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.registry.PlayerSkillBase;

public class SkillEnderManipulator extends PlayerSkillBase
{
	public SkillEnderManipulator()
	{
		super(5);
		setRegistryName(InfoIS.MOD_ID, "ender_manipulator");
		
		hasScroll = true;
		genScroll = true;
		
		getLoot().chance.n = 20;
		getLoot().setLootTable(LootTableList.ENTITIES_ENDERMAN);
		
		xpCalculator.xpValue = 3;
	}
}