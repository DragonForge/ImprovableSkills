package org.zeith.improvableskills.custom.pagelets;

import com.zeitheron.hammercore.client.HCClientOptions;
import com.zeitheron.hammercore.lib.zlib.utils.MD5;
import com.zeitheron.hammercore.lib.zlib.web.HttpRequest;
import com.zeitheron.hammercore.lib.zlib.web.HttpRequest.HttpRequestException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PageletBase;
import org.zeith.improvableskills.client.gui.GuiNewsBook;

import java.nio.charset.StandardCharsets;

public class PageletNews extends PageletBase
{
	public final ResourceLocation texture = new ResourceLocation(InfoIS.MOD_ID, "textures/gui/news.png");
	
	{
		setRegistryName(InfoIS.MOD_ID, "news");
		setTitle(new TextComponentTranslation("pagelet." + InfoIS.MOD_ID + ":news"));
	}
	
	@Override
	public boolean isRight()
	{
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Object getIcon()
	{
		Object o = super.getIcon();
		
		if(!(o instanceof ITextureObject))
		{
			Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
			setIcon(o = Minecraft.getMinecraft().getTextureManager().getTexture(texture));
		}
		
		return o;
	}
	
	boolean popping = true;
	String changes;
	
	public String getChanges()
	{
		return changes;
	}
	
	@Override
	public void reload()
	{
		popping = true;
		
		try
		{
			this.changes = new String(HttpRequest.get("https://mods.zeith.org/improvableskills/news.txt?mc=1.12.2").userAgent("ImprovableSkills v" + InfoIS.MOD_VERSION).connectTimeout(5000).bytes(), StandardCharsets.UTF_8);
			String rem = MD5.encrypt(changes);
			
			HCClientOptions opts = HCClientOptions.getOptions();
			NBTTagCompound nbt = opts.getCustomData();
			String stored = nbt.getString("ImprovableSkillsNewsMD5");
			
			if(stored.equalsIgnoreCase(rem))
				popping = false;
		} catch(HttpRequestException ignored)
		{
		}
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean doesPop()
	{
		return popping;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public GuiNewsBook createTab(PlayerSkillData data)
	{
		return new GuiNewsBook(this);
	}
}