package org.zeith.improvableskills.custom.pagelets;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.SimpleTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.PlayerSkillData;
import org.zeith.improvableskills.api.registry.PageletBase;
import org.zeith.improvableskills.client.gui.GuiDiscord;

public class PageletDiscord extends PageletBase
{
	public final ResourceLocation texture = new ResourceLocation(InfoIS.MOD_ID, "textures/gui/discord.png");
	Object staticIcon;
	
	{
		setRegistryName(InfoIS.MOD_ID, "discord");
		setTitle(new TextComponentTranslation("pagelet." + InfoIS.MOD_ID + ":discord1"));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public GuiDiscord createTab(PlayerSkillData data)
	{
		return new GuiDiscord(this);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Object getIcon()
	{
		Object o = staticIcon;
		
		{
			TextureManager mgr = Minecraft.getMinecraft().getTextureManager();
			
			ITextureObject itextureobject = mgr.getTexture(texture);
			
			if(itextureobject == null)
			{
				itextureobject = new SimpleTexture(texture);
				mgr.loadTexture(texture, itextureobject);
			}
			
			staticIcon = o = itextureobject;
		}
		
		return o;
	}
	
	@Override
	public boolean isRight()
	{
		return false;
	}
}