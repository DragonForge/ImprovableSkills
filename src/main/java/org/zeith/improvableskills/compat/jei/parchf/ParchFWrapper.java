package org.zeith.improvableskills.compat.jei.parchf;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;
import org.zeith.improvableskills.api.RecipesParchmentFragment;
import org.zeith.improvableskills.init.ItemsIS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ParchFWrapper implements IRecipeWrapper
{
	public final RecipesParchmentFragment.RecipeParchmentFragment recipe;
	
	public ParchFWrapper(RecipesParchmentFragment.RecipeParchmentFragment recipe)
	{
		this.recipe = recipe;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		List<List<ItemStack>> inputs = new ArrayList<>(recipe.itemsIn.stream().map(c -> Arrays.stream(c.ingr.getMatchingStacks()).map(ItemStack::copy).collect(Collectors.toList())).collect(Collectors.toList()));
		inputs.add(Arrays.asList(new ItemStack(ItemsIS.PARCHMENT_FRAGMENT)));
		ingredients.setInputLists(ItemStack.class, inputs);
		ingredients.setOutput(ItemStack.class, recipe.output.copy());
	}
}