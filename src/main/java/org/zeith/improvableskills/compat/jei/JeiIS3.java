package org.zeith.improvableskills.compat.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.recipe.IRecipeCategoryRegistration;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;
import net.minecraft.item.ItemStack;
import org.zeith.improvableskills.InfoIS;
import org.zeith.improvableskills.api.RecipesParchmentFragment;
import org.zeith.improvableskills.client.gui.abil.crafter.ContainerCrafter;
import org.zeith.improvableskills.client.gui.abil.crafter.GuiCrafter;
import org.zeith.improvableskills.compat.jei.parchf.ParchFCategory;
import org.zeith.improvableskills.compat.jei.parchf.ParchFWrapper;
import org.zeith.improvableskills.init.ItemsIS;

@JEIPlugin
public class JeiIS3 implements IModPlugin
{
	public static final String PARCHF_CAT = InfoIS.MOD_ID + ":parchf";
	
	@Override
	public void register(IModRegistry registry)
	{
		registry.addRecipeCatalyst(new ItemStack(ItemsIS.SKILLS_BOOK), PARCHF_CAT);
		registry.handleRecipes(RecipesParchmentFragment.RecipeParchmentFragment.class, ParchFWrapper::new, PARCHF_CAT);
		
		registry.addRecipes(RecipesParchmentFragment.RECIPES, PARCHF_CAT);
		
		registry.addRecipeClickArea(GuiCrafter.class, 88, 32, 28, 23, VanillaRecipeCategoryUid.CRAFTING);
		registry.getRecipeTransferRegistry().addRecipeTransferHandler(ContainerCrafter.class, VanillaRecipeCategoryUid.CRAFTING, 1, 9, 10, 36);
	}
	
	@Override
	public void registerCategories(IRecipeCategoryRegistration registry)
	{
		registry.addRecipeCategories(new ParchFCategory(registry.getJeiHelpers()));
	}
}